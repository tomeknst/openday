package com.acaisoft.openday.schema;

/**
 * Created by vagrant on 5/17/17.
 */
public class Booking {
    private String username;
    private String bookId;

    public Booking(String username, String bookId) {
        this.username = this.username;
        this.bookId = bookId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
}
