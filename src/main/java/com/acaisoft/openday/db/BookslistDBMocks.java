package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookslistDBMocks {

    public static List<Book> getMocks() {
        List<Book> booksList = new ArrayList<>(10);
        booksList.addAll(Arrays.asList(
            new Book()
                .setId("1")
                .setAuthor("Glen Cook")
                .setTitle("The Black Company")
                .setDescription("Good fantasy tales rely on world building, and Glen Cook’s Black Company series has this in spades.")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/BlackCompany.jpg")
                .setQuantity(14),
            new Book()
                .setId("2")
                .setAuthor("Mark Lawrence")
                .setTitle("The Broken Empire Trilogy")
                .setDescription("Mark Lawrence’s Broken Empire Trilogy wraps a war epic around a family drama within a coming-of-age story, creating a multi-layered fantasy series steeped in dark magic. ")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/BrokenEmpire.jpg")
                .setQuantity(5),
            new Book()
                .setId("3")
                .setAuthor("C.S. Lewis")
                .setTitle("The Chronicles of Narnia")
                .setDescription("C.S. Lewis’ kindhearted Narnia series sometimes feels like the yang to Tolkien’s serious and moody yin, which makes sense, given that the two Inklings were close friends for decades.")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/ChroniclesofNarnia.jpg")
                .setQuantity(4),
            new Book()
                .setId("4")
                .setAuthor("Lloyd Alexander")
                .setTitle("The Chronicles of Prydain")
                .setDescription("In an age when any successful tween series is hyped as a potential film franchise, Lloyd Alexander’s pentalogy has managed to stay out of the limelight.")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/ChroniclesofPrydain.jpg")
                .setQuantity(44),
            new Book()
                .setId("5")
                .setAuthor("R.A. Salvatore")
                .setTitle("The Dark Elf Trilogy")
                .setDescription("Role-play gaming and fantasy novels have gone hand-in-hand for most of their existence, but it’s a path that can be dangerous for casual fans to explore.")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/DarkElf.jpg")
                .setQuantity(17),
            new Book()
                .setId("6")
                .setAuthor("Stephen King")
                .setTitle("The Dark Tower")
                .setDescription("You probably associate Stephen King’s name with another brand of genre fiction, but that’s no reason to discount his forays past traditional horror. ")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/DarkTower.jpeg")
                .setQuantity(44),
            new Book()
                .setId("7")
                .setAuthor("Discworld")
                .setTitle("Terry Pratchett")
                .setDescription("Terry Pratchett was a voice sorely needed in the regal, sonorous fantasy genre, where so many authors have imagined themselves as the next Tolkienesque figure.")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/Discworld.jpg")
                .setQuantity(1),
            new Book()
                .setId("8")
                .setAuthor("Anne McCaffrey")
                .setTitle("Dragonriders")
                .setDescription(" Great fantasy writing teases the imagination, and I still remember the feeling of reading those first Pern books as a child—the wonder of dragons who bonded with their riders. ")
                .setImageUrl("http://cdn.pastemagazine.com/www/articles/Dragonriders.jpg")
                .setQuantity(12)));

        return booksList;
    }
}
