package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Book;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BookslistDB {
    private List<Book> booksList;

    public BookslistDB() {
        this.booksList = new ArrayList<>();
        this.booksList = BookslistDBMocks.getMocks();
    }

    public List<Book> getBooksList() {
        return this.booksList;
    }

    public boolean bookExists(String bookId) {
        Optional<Book> book = booksList.stream().
                                        filter((b) -> b.getId() == bookId).
                                        findFirst();
        return book.isPresent();
    }

    public Optional<Book> getBook(String bookId) {
        Optional<Book> book = booksList.stream().
                                        filter((b) -> b.getId() == bookId).
                                        findFirst();
        return book;
    }
}
