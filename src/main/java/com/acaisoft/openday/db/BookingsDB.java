package com.acaisoft.openday.db;

import com.acaisoft.openday.schema.Book;
import com.acaisoft.openday.schema.Booking;
import com.acaisoft.openday.schema.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vagrant on 5/17/17.
 */

@Service
public class BookingsDB {
    private List<Booking> bookings;

    public BookingsDB() {
        this.bookings = new ArrayList<>();
    }

    public void borrowBook(User user, Book book) {
        this.bookings.add(new Booking(user.getUsername(), book.getId()));
    }

    public void returnBook(User user, Book book) {
        String username = user.getUsername();
        String bookId = book.getId();
        this.bookings.remove(this.bookings.stream().
                                           filter((booking)->booking.getBookId()==bookId &&
                                                   booking.getUsername()==username).findFirst());

    }

    public long countBorrowed(Book book) {
        String bookId = book.getId();
        return this.bookings.stream().filter((booking)->booking.getBookId()==bookId).count();
    }
}
