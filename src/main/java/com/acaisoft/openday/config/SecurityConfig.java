package com.acaisoft.openday.config;

import com.acaisoft.openday.db.UsersDB;
import com.acaisoft.openday.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.authentication.AuthenticationManagerBeanDefinitionParser;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String LOGIN_PATH = "/api/user/login";
    private static final String SIMPLE_REGISTER_PATH = "/api/user/register/simple";
    private static final String CAPTCHA_REGISTER_PATH = "/api/user/register/captcha";
    private static final String GENERATE_CAPTCHA_PATH = "/api/user/register/captcha/generate";
    private static final String DOES_USER_EXIST_PATH = "/api/user/exists/*";

    @Autowired
    private JwtFactory jwtFactory;

    @Autowired
    private AuthenticationExceptionHandler authenticationExceptionHandler;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private UsersDB usersDB;


    @Override
    @Autowired
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder
            .authenticationProvider(new AuthenticationManagerBeanDefinitionParser.NullAuthenticationProvider())
        ;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(jwtFactory, authenticationExceptionHandler);

        UsernamePasswordFilter passwordAuthenticationFilter = new UsernamePasswordFilter(
            new AntPathRequestMatcher(LOGIN_PATH, "POST"),
            usersDB,
            jwtFactory,
            authenticationExceptionHandler,
            authenticationSuccessHandler
        );

        http
            .authorizeRequests()
            .antMatchers("/", "/app/**", "/assets/**",
                "/fonts/**", "/favicon.ico", "/*bundle.js", "/*bundle.css", "/*bundle.map", "/*.woff", "/*.woff2", "/*.ttf").permitAll()
            .antMatchers(LOGIN_PATH, SIMPLE_REGISTER_PATH, CAPTCHA_REGISTER_PATH, GENERATE_CAPTCHA_PATH, DOES_USER_EXIST_PATH).permitAll()
            .anyRequest().hasAnyAuthority("SIMPLE_USER")
            .and()
            .addFilterAfter(jwtAuthenticationFilter, SecurityContextPersistenceFilter.class)
            .addFilterAfter(passwordAuthenticationFilter, JwtAuthenticationFilter.class)
            .formLogin().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;
    }
}
