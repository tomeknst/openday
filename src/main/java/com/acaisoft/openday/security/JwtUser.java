package com.acaisoft.openday.security;


import org.springframework.security.core.userdetails.UserDetails;

public interface JwtUser extends UserDetails{

    String token() throws Exception;

    class CLAIMS_PARAM_NAMES {
        public static final String USER_NAME = "email";
        public static final String ROLES = "roles";
    }
}
