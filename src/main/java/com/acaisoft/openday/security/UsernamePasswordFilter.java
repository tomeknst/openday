package com.acaisoft.openday.security;


import com.acaisoft.openday.AppException;
import com.acaisoft.openday.db.UsersDB;
import com.acaisoft.openday.schema.User;
import com.acaisoft.openday.security.impl.UserDetailsByUsername;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

public class UsernamePasswordFilter extends OncePerRequestFilter {

    private Gson gson = new Gson();

    private RequestMatcher requiresAuthenticationRequestMatcher;
    private UsersDB usersDB;
    private JwtFactory jwtFactory;
    private AuthenticationExceptionHandler exceptionHandler;
    private AuthenticationSuccessHandler successHandler;

    public UsernamePasswordFilter(RequestMatcher requiresAuthenticationRequestMatcher,
                                  UsersDB usersDB,
                                  JwtFactory jwtFactory,
                                  AuthenticationExceptionHandler exceptionHandler,
                                  AuthenticationSuccessHandler successHandler) {
        this.requiresAuthenticationRequestMatcher = requiresAuthenticationRequestMatcher;
        this.usersDB = usersDB;
        this.jwtFactory = jwtFactory;
        this.exceptionHandler = exceptionHandler;
        this.successHandler = successHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        try {


            if(!requiresAuthenticationRequestMatcher.matches(request)){
                chain.doFilter(request, response);
                return;
            }

            UserJson user = parseRequest(request);

            UserDetailsByUsername userDetails = userDetails(user);
            PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(userDetails.getUsername(), "", userDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String token = jwtFactory.jwt().token(userDetails);
            successHandler.onAuthenticationSuccess(request, response, token);
        }catch (Exception ex){
            exceptionHandler.onAuthenticationFailure(request, response, ex);
        }


    }

    private UserDetailsByUsername userDetails(UserJson userJson) throws Exception{
        Optional<User> userOption =usersDB.getUser(userJson.email);

        if(!userOption.isPresent()){
            throw new AppException("User doesn't exists");
        }

        User user = userOption.get();

        if(!StringUtils.equals(user.getPassword(), userJson.password)){
            throw new AppException("Username or password is incorrect");
        }

        return new UserDetailsByUsername(user.getUsername(), Arrays.asList(new SimpleGrantedAuthority("SIMPLE_USER")));
    }


    private UserJson parseRequest(HttpServletRequest request) throws Exception{
        try(InputStream inputStream = request.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8) ){
            String json = CharStreams.toString(inputStreamReader);
            return gson.fromJson(json, UserJson.class);
        } catch (Exception e) {
            throw new AppException("Failed to parse", e);
        }
    }


    private static class UserJson {
        public String email;
        public String password;
    }
}
