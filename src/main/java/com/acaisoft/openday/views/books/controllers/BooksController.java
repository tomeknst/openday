package com.acaisoft.openday.views.books.controllers;

import com.acaisoft.openday.db.BookingsDB;
import com.acaisoft.openday.db.BookslistDB;
import com.acaisoft.openday.db.BookslistDBException;
import com.acaisoft.openday.db.UsersDB;
import com.acaisoft.openday.schema.Book;
import com.acaisoft.openday.schema.User;
import com.acaisoft.openday.views.user.dto.CaptchaRegisterUserDto;
import com.acaisoft.openday.views.user.dto.LoginDto;
import com.acaisoft.openday.views.user.dto.SimpleRegisterUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BooksController {
    private BookslistDB bookslistDB;
    private UsersDB usersDB;
    private BookingsDB bookingsDB;

    @Autowired
    public BooksController(BookslistDB bookslistDB, UsersDB usersDB, BookingsDB bookingsDB) {
        this.bookslistDB = bookslistDB;
        this.usersDB = usersDB;
        this.bookingsDB = bookingsDB;
    }

    @GetMapping("/bookslist")
    public ResponseEntity<List<Book>> getBooksList() {
        return ResponseEntity.ok(this.bookslistDB.getBooksList());
    }

    @PostMapping("/bookslist/{bookid}/borrow")
    public ResponseEntity borrowBook(@RequestBody @Valid LoginDto userData, @PathVariable("bookid") String bookid) {
        boolean validCredentials = usersDB.areCredentialsValid(userData.getLogin(), userData.getPassword());
        boolean validBookId = bookslistDB.bookExists(bookid);

        if(validCredentials && validBookId) {
            User user = usersDB.getUser(userData.getLogin()).get();
            Book book = bookslistDB.getBook(bookid).get();

            if(book.getQuantity() > bookingsDB.countBorrowed(book)) {
                bookingsDB.borrowBook(user, book);
                return ResponseEntity.ok().build();
            } else
                return ResponseEntity.status(195).build();
        }
        else {
            if (!validBookId)
                return ResponseEntity.status(190).build();
            else
                return ResponseEntity.status(185).build();
        }
    }
}
