package com.acaisoft.openday.views.user.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by vagrant on 5/17/17.
 */
public class LoginDto {
    @NotNull
    private String login;
    @NotNull
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
